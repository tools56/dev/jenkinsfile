#!/bin/bash

file_cfg=$1

numColIndex=4

Rc=0
RcIndex=0
nameIndex=""

elastic=$(kubectl get pods --all-namespaces --field-selector=status.phase=Running  --selector=app=pwc-elasticsearch -o jsonpath="{range .items[*]}{.metadata.namespace}={.status.hostIP}{'\n'}" )
RcIp=$?
if [[ ${Rc} -eq 0 ]]; then
    ip_elastic=$( echo ${elastic} | tr '\n' ' ' | cut -d' ' -f1 | cut -d'=' -f2 )
    namespace=$( echo ${elastic} | tr '\n' ' ' | cut -d' ' -f1 | cut -d'=' -f1 )
fi

port_elastic=$(kubectl get svc -n ${namespace} -o jsonpath="{range .items[?(@.metadata.name=='pwc-elasticsearch')]}{.spec.ports[0].nodePort}{'\n'}" )
RcPort=$?
Rc=$(( ${Rcip} + ${RcPort} ))

if [[ ${Rc} -eq 0 ]]; then
   port_elastic=$( echo ${port_elastic} | tr '\n' ' ' | cut -d' ' -f1)
   if [[ "${pwc_elasticsearch_Login// }" == "" ]] || [[ "${pwc_elasticsearch_Password// }" == "" ]]; then
      RcIndex=69
   else
      nameIndex=$(curl -s -k --user "${pwc_elasticsearch_Login}:${pwc_elasticsearch_Password}"  "https://${ip_elastic}:${port_elastic}/_cat/indices" | cut -d\  -f${numColIndex} | grep "pwc-authorization-auth" | head -1)
      RcIndex=$?
      if [[ ${RcIndex} -eq 0 ]] && [[ "${nameIndex// }" == "" ]]; then
           RcIndex=68
      else
         Rc=$(( ${Rcip} + ${RcPort} + ${RcIndex} ))
      fi
   fi
fi

if [[ ${RcIndex} -ne 0 ]]; then
   echo "Error for search pwc_elasticsearch_Index (${RcIndex})"
   Rc=33
fi

if [[ ${Rc} -eq 0 ]]; then
   if [[ "${file_cfg// }" == "" ]]; then
      echo "pwc_elasticsearch=\"https://${ip_elastic}:${port_elastic}\""
      echo "pwc_elasticsearch_Index=\"${nameIndex}\""
   else
      echo "export pwc_elasticsearch_IP=\"https://${ip_elastic}\"" > "${file_cfg}"
      echo "export pwc_elasticsearch_Port=\"${port_elastic}\"" >> "${file_cfg}"
      echo "export pwc_elasticsearch_Index=\"${nameIndex}\"" >> "${file_cfg}"
   fi
fi

exit $Rc
